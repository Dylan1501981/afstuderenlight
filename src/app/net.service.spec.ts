import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NetService } from './net.service';
import { HttpClient } from '@angular/common/http';

describe('NetService', () => {
  let service: NetService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [ NetService ]
    });
    service = TestBed.inject(NetService);
  });


  it('should be created', () => {
    expect(service).toBeTruthy();
  });

});
