import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NetService {
  apiURL: string = 'https://afstuderenlightavans.azurewebsites.net/api';
  constructor(private httpClient: HttpClient) { }

  public getFibonacciCalculation(n: number) {

    const reqObj = {
      Number: n
    };

    return this.httpClient.post(`${this.apiURL}/number/fibonacci`, reqObj);
  }
}


