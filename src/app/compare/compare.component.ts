import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { WasmService } from 'src/wasm.service';
import { NetService } from '../net.service';
import { AlgorithmResult } from '../algorithm-result';
import { Subject } from 'rxjs';
import 'datatables.net';

@Component({
  selector: 'app-compare',
  templateUrl: './compare.component.html',
  styleUrls: ['./compare.component.css']
})
export class CompareComponent implements OnInit, OnDestroy {
  fibonacciResult: number;
  fibonacciNETResult: number;
  results: AlgorithmResult[] = [];
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<AlgorithmResult> = new Subject();
  WASM_N: number;
  NET_N: number;

  constructor(private wasmService: WasmService, private net: NetService) {

  }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 2,
    };

  }
  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  measurePerformance(): number {
    return performance.now();
  }

  renderTable(): void {
    this.dtTrigger.next();
  }

  submitWASMFibonacci(): void {
    this.executeWASM(this.WASM_N);
  }

  submitWASMNet(): void {
    this.executeNET(this.NET_N);
  }

  executeNET(n: number): void {
    const t0 = this.measurePerformance();
    this.net.getFibonacciCalculation(n).subscribe((result) => {
      const t1 = this.measurePerformance();
      console.log(result);
      const entry = new AlgorithmResult();
      entry.n = n;
      entry.type = 'Fibonacci - NET';
      entry.exec_time = result['executionTime'];
      entry.response_time = t1 - t0;
      entry.result = result['result'];
      this.results.push(entry);
    });
  }

  executeWASM(n: number): void {
    const t0 = this.measurePerformance();
    this.wasmService.fibonacci(n).subscribe(result => {
      const t1 = this.measurePerformance();
      this.fibonacciResult = result;
      const entry = new AlgorithmResult();
      entry.n = n;
      entry.result = result;
      entry.type = 'Fibonacci - WASM';
      entry.exec_time = t1 - t0;
      entry.response_time = t1 - t0;
      this.results.push(entry);
    });
  }
}
