export class AlgorithmResult {
  result: number;
  n: number;
  type: string;
  exec_time: number;
  response_time: number;
}
