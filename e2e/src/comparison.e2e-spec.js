describe('Comparison Tests', function() {
  it ('RESPONSE TIME: Checks what is faster', function() {
    browser.get("/");
    element(by.name('NET_N')).sendKeys('9999');
    element(by.name('WASM_N')).sendKeys('9999');

    var buttonNET = element(by.name('btnNet'))
    browser.wait(protractor.ExpectedConditions.elementToBeClickable(buttonNET), 30000)
    .then ( function () {
      buttonNET.click();
    });

    var buttonWASM = element(by.name('btnWASM'))
    browser.wait(protractor.ExpectedConditions.elementToBeClickable(buttonWASM), 30000)
    .then ( function () {
      buttonWASM.click();
    })

    var tabledata = element.all(by.name("DataTable"));

    // get rows
    var rows = tabledata.all(by.tagName("tr"));

    // get cell values
    var cells = rows.all(by.tagName("td"));

    cells.get(3).getText().then(function(text) {
      console.log("Execution time of NET Webservice was: " + text + " ms")

    });

    cells.get(8).getText().then(function(text) {
      console.log("Execution time of WASM was: " + text + " ms")
    })
  })

  it ('EXECUTION TIME: Checks what is faster', function() {
    browser.get("/");
    element(by.name('NET_N')).sendKeys('9999');
    element(by.name('WASM_N')).sendKeys('9999');

    var buttonNET = element(by.name('btnNet'))
    browser.wait(protractor.ExpectedConditions.elementToBeClickable(buttonNET), 30000)
    .then ( function () {
      buttonNET.click();
    });

    var buttonWASM = element(by.name('btnWASM'))
    browser.wait(protractor.ExpectedConditions.elementToBeClickable(buttonWASM), 30000)
    .then ( function () {
      buttonWASM.click();
    })

    var tabledata = element.all(by.name("DataTable"));

    // get rows
    var rows = tabledata.all(by.tagName("tr"));

    // get cell values
    var cells = rows.all(by.tagName("td"));

    cells.get(4).getText().then(function(text) {
      console.log("Response time of NET Webservice was: " + text + " ms")

    });

    cells.get(9).getText().then(function(text) {
      console.log("Response time of WASM was: " + text + " ms")
    })
  })
});
