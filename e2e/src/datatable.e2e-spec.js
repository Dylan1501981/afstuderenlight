describe('Front-End JavaScript tests', function() {
  it ('should determine that the JavaScript front-end library DataTables is still working', function() {
    browser.get("/");
    element(by.name('NET_N')).sendKeys('9999');
    element(by.name('WASM_N')).sendKeys('9999');

    var buttonNET = element(by.name('btnNet'))
    browser.wait(protractor.ExpectedConditions.elementToBeClickable(buttonNET), 30000)
    .then ( function () {
      buttonNET.click();
    });

    var buttonWASM = element(by.name('btnWASM'))
    browser.wait(protractor.ExpectedConditions.elementToBeClickable(buttonWASM), 30000)
    .then ( function () {
      buttonWASM.click();
    });

    var buttonRenderTable = element(by.name('btnRenderTable'))
    browser.wait(protractor.ExpectedConditions.elementToBeClickable(buttonRenderTable), 50000)
    .then ( function () {
      buttonRenderTable.click();
      var table = element(by.name('DataTable'))
      expect(table.getAttribute('class')).toMatch('row-border hover dataTable no-footer');
    });
  })
});
