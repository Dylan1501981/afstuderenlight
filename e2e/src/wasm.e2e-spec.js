describe('WASM Tests', function() {
  it ('WASM: should report the execution and response time of the fibonnacci algorithm where n is 9999', function() {
    browser.get("/");
    element(by.name('WASM_N')).sendKeys('9999');

    var buttonWASM = element(by.name('btnWASM'))
    browser.wait(protractor.ExpectedConditions.elementToBeClickable(buttonWASM), 30000)
    .then ( function () {
      buttonWASM.click();
    });

    var tabledata = element.all(by.name("DataTable"));

    // get rows
    var rows = tabledata.all(by.tagName("tr"));

    // get cell values
    var cells = rows.all(by.tagName("td"));


    cells.get(0).getText().then(function(text) {
      console.log("==== WASM FIBONACCI TEST RESULT (N IS 9999) ====")
      console.log("TYPE: " + text);
    });

    cells.get(1).getText().then(function(text) {
      console.log("N: " + text);
    });

    cells.get(2).getText().then(function(text) {
      console.log("RESULT: " + text);
    });

    cells.get(3).getText().then(function(text) {
      console.log("EXECUTION TIME IN MS: " + text);
    });

    cells.get(4).getText().then(function(text) {
      console.log("RESPONSE TIME IN MS (NETWORK LATENCY INCLUDED): " + text);
      console.log("==== END WASM TEST RESULT ====")
    });
  });
});
